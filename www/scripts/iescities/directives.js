/* Directives */
var iescitiesAppDirectives = angular.module('iescitiesAppDirectives', []);

/** Zaragoza map directive */
iescitiesAppDirectives
		.directive(
				'zaragozaMap',
				[
						'$window',
						'$rootScope',
						'$parse',
						'mapsService',
						function($window, $rootScope, $parse, mapsService) {

							return {
								restrict : 'A',
								replace : false,
								template : '<div id="map_canvas" style="height: 600px;"></div>',
								link : function($scope, element, attrs) {

									var mapDiv = document
											.getElementById("map_canvas"), bounds, mapOptions, map, resolutionList, center, currentPois = [];
									$(mapDiv).width($(window).width());
									$(mapDiv).height($(window).height());

									// TOOD: IesCities Server session ID
									var max = 9999999;
									var min = 0;
									$rootScope.sessionId = Math.floor(Math
											.random()
											* (max - min + 1) + min);

									// World Geodetic System 1984 projection
									// (lon/lat)
									var WGS84 = new OpenLayers.Projection(
											"EPSG:4326");

									// WGS84 Google Mercator projection (meters)
									var WGS84_google_mercator = new OpenLayers.Projection(
											"EPSG:900913");

									/** Openlayers onMapClick Event */
									OpenLayers.Control.Click = OpenLayers
											.Class(
													OpenLayers.Control,
													{
														defaultHandlerOptions : {
															'single' : true,
															'double' : false,
															'pixelTolerance' : 0,
															'stopSingle' : false,
															'stopDouble' : false
														},

														initialize : function(
																options) {
															this.handlerOptions = OpenLayers.Util
																	.extend(
																			{},
																			this.defaultHandlerOptions);
															OpenLayers.Control.prototype.initialize
																	.apply(
																			this,
																			arguments);
															this.handler = new OpenLayers.Handler.Click(
																	this,
																	{
																		'click' : this.trigger
																	},
																	this.handlerOptions);
														},

														// onMapClick
														trigger : function(e) {
															closePoiInfo();
														}
													});

									OpenLayers.ProxyHost = "proxy.cgi?url=";
									Proj4js.defs["EPSG:23030"] = "+proj=utm +zone=30 +ellps=intl +towgs84=-131,-100.3,-163.4,-1.244,-0.020,-1.144,9.39 +units=m +no_defs";
									resolutionList = [ 53.125201382285255,
											26.562600691142627,
											14.062553307075499,
											6.718775468936064,
											3.7500142152201517,
											1.7187565153092277,
											0.9375035538050343,
											0.5000018953626857,
											0.26375099980381617 ];

									// WMS Estandar:
									// http://idezar.zaragoza.es/wms/IDEZar_base/IDEZar_base

									// WMS-C (Tileado):
									// http://idezar.zaragoza.es/IDEZar_Base_Tiled/WMSTileCache?&SERVICE=WMS&REQUEST=GetCapabilities

									// WMTS Estandar (Tileado)
									// http://idezar.zaragoza.es/IDEZar_Base_WMTS/TileCache

									var tiles = "http://idezar.zaragoza.es/IDEZar_Base_Tiled/WMSTileCache";
									var sin = "http://idezar.zaragoza.es/wms/IDEZar_base/IDEZar_base";

									var idezar = new OpenLayers.Layer.WMS(
											"IDEZar",
											"http://idezar.zaragoza.es/IDEZar_Base_Tiled/WMSTileCache",
											{
												version : '1.1.1',
												layers : 'base',
												format : 'image/png',
												transparent : false,
											},
											{
												projection : new OpenLayers.Projection(
														"EPSG:23030"),
												resolutions : [
														53.125201382285255,
														26.562600691142627,
														14.062553307075499,
														6.718775468936064,
														3.7500142152201517,
														1.7187565153092277,
														0.9375035538050343,
														0.5000018953626857,
														0.26375099980381617,
														0.131875499901908 ]
											});

									// TODO: Add parameters to match correct map
									// projection and conversion
									// var googleMapsLayer = new
									// OpenLayers.Layer.Google(
									// "Google Maps", // the default
									// {
									// numZoomLevels : 20,
									// });

									var markers = new OpenLayers.Layer.Markers(
											"POIs");

									map = new OpenLayers.Map(
											{
												div : "map_canvas",
												layers : [ idezar, markers /* ,googleMapsLayer */],
												theme : null,
												resolutions : resolutionList,
												projection : new OpenLayers.Projection(
														"EPSG:23030"),
												displayProjection : new OpenLayers.Projection(
														"EPSG:4326"),
												maxExtent : new OpenLayers.Bounds(
														651500, 4590500,
														694100, 4645000),
												maxResolution : 53.125201382285255,
												controls : [
														new OpenLayers.Control.Navigation(),
														new OpenLayers.Control.ArgParser(),
														new OpenLayers.Control.Attribution() ]
											});

									// Enable map click event
									var click = new OpenLayers.Control.Click();
									map.addControl(click);
									click.activate();

									// When map is loaded
									idezar.events
											.register(
													"loadend",
													idezar,
													function() {
														// Load public maps
														mapsService
																.getPublicMaps(function(
																		data) {
																	$rootScope.publicMaps = data;
																});
													});

									map
											.addControl(new OpenLayers.Control.MousePosition());
									// map.zoomToMaxExtent();
									map.setCenter(new OpenLayers.LonLat(676171,
											4613051), 2);

									// TODO: Add Layer Switcher when Google Maps
									// projection fixed
									// map.addControl(new
									// OpenLayers.Control.LayerSwitcher());

									/** Rest logging service */
									Restlogging
											.init("https://iescities.com:443");

									/** RootScope variables */
									$rootScope.helpVisible = false;
									$rootScope.deleteMapDialogVisible = false;
									$rootScope.isMapEditable = false;
									$rootScope.newMapTitle = null;
									$rootScope.selectedMap = [];
									$rootScope.selectedPoi = null;
									$rootScope.newPois = [];
									$rootScope.updatedPois = [];
									$rootScope.deletedPois = [];

									/** Scope variables */
									$scope.userMaps = [];

									/** LISTENERS */
									// If selected map changes, show selected
									// map POIs
									// $scope.$watch('selectedMap', function() {
									// if ($rootScope.selectedMap.id != null) {
									// updateMapPois();
									// }
									// }, true);
									/** PUBLIC METHODS */
									$scope.removePoiFromMap = function(poi) {
										$rootScope.poiMarkers
												.forEach(function(marker) {
													if (marker.poi == poi) {
														var index = $rootScope.poiMarkers
																.indexOf(marker);
														if (index > -1) {
															// Delete map marker
															markers
																	.removeMarker($rootScope.poiMarkers[index]);
															$rootScope.poiMarkers
																	.splice(
																			index,
																			1);
															$rootScope.deletedPois
																	.push(marker.poi);

															// Delete POI from
															// newPois if exists
															var index2 = $rootScope.newPois
																	.indexOf(marker.poi);
															if (index2 > -1) {
																$rootScope.newPois
																		.splice(
																				index2,
																				1);
															}
														}
													}
												});
										$rootScope.selectedPoi = null;
									};

									$scope.refreshMapData = function(
											selectedMap) {
										
										if (selectedMap != undefined && !selectedMap.owned){
											Restlogging.appLog(logType.CONSUME,"consulting not owned map");
										}
										
										$rootScope.selectedPoi = null;
										$rootScope.selectedMap = selectedMap;
										$rootScope.mapBackup = selectedMap;
										$rootScope.isMapEditModeActivated = false;

										// Temp variables to handle map`s pois
										// changes
										$rootScope.newPois = [];
										$rootScope.updatedPois = [];
										$rootScope.deletedPois = [];

										if (selectedMap.type == "collaborative"
												|| $rootScope.isUserPanel) {
											$rootScope.isMapEditable = true;
										} else {
											$rootScope.isMapEditable = false;
										}
										
										if (selectedMap.id != null) {
											updateMapPois();
										}

										// var bounds = markers.getExtent();
										// if (bounds != null) {
										// map.zoomToExtent(bounds);
										// }

									};

									$scope.updateUserMaps = function(maps) {
										$scope.userMaps = maps;
									};

									// Not used: Resizes map according to window
									// size
									$scope.resizeMap = function() {
										$(mapDiv).width($(window).width());
										$(mapDiv).height($(window).height());
									};

									$scope.centerMapInZaragoza = function() {
										map.setCenter(new OpenLayers.LonLat(
												676171, 4613051), 2);
									};

									$scope.centerMapInPosition = function(pos) {
										document
												.getElementById("my_location_btn").className = "my_location_btn_normal ng-scope";
										document
												.getElementById("my_location_btn").disabled = false;
										var mapBounds = map.getExtent();
										var center = new OpenLayers.LonLat(
												pos.coords.longitude,
												pos.coords.latitude);
										center.transform(
												new OpenLayers.Projection(
														"EPSG:4326"), map
														.getProjectionObject());
										map.setCenter(center, 5);
										// if (mapBounds.containsLonLat(center))
										// {
										// Center only if current position is
										// inside the map
										// map.setCenter(center, 5);
										// } else {
										// toastr.info("Su ubicación actual
										// está
										// fuera del mapa de Zaragoza");
										// }
									};

									// Close panel
									$scope.closePanel = function(panelId) {
										if (isWebKit()) {
											document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
										} else {
											document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
										}
										document.getElementById(panelId).style.zIndex = 999;
									};

									// EditPanel - Add new poi to map
									$scope.addNewPoiToMap = function(title,
											description) {
										createNewPoiAndMarker(title,
												description);
									};

									// EditPanel - Cancel map changes
									$scope.cancelMapChanges = function() {
										$rootScope.selectedMap = angular
												.copy($rootScope.mapBackup);
										updateMapPois();
									};

									/** PRIVATE METHODS */
									function updateMapPois() {
										$rootScope.poiMarkers = [];
										markers.destroy();
										markers = new OpenLayers.Layer.Markers(
												"POIs");
										map.addLayer(markers);
										mapsService
												.getMapPoisAuthenticated(
														$rootScope.selectedMap.id)
												.success(
														function(data) {
															if (data.totalCount != null) {
																data.result
																		.forEach(function(
																				poi) {
																			addPoiToMap(
																					poi,
																					true,
																					true);
																		});
															} else {
																toastr
																		.info("Mapa seleccionado: "
																				+ $rootScope.selectedMap.title);
															}
														}).error(
														function(data) {
															// toastr.info("Error
															// al obtener
															// información del
															// mapa: " +
															// data.mensaje);
														});

									}

									function addPoiToMap(poi, closeBox,
											overflow) {
										currentPois.push(poi);
										var lat = poi.geometry.coordinates[1];
										var lon = poi.geometry.coordinates[0];
										var point = new OpenLayers.LonLat(lon,
												lat).transform(
												new OpenLayers.Projection(
														"EPSG:4326"),
												new OpenLayers.Projection(
														"EPSG:23030"));

										popupClass = OpenLayers.Popup.FramedCloud;
										popupContentHTML = '<div id ="'
												+ poi.id
												+ '" class="pop_theme">'
												+ '<span class="popup_title"> '
												+ poi.title
												+ ' (ID: '
												+ poi.id
												+ ')</span><br>'
												+ '<span class="popup_status"> Estado: '
												+ poi.title + '</span><br><br>'
												+ '<span>' + poi.description
												+ '</span><br>' + '<br></div>';

										var size = new OpenLayers.Size(30, 40);
										var offset = new OpenLayers.Pixel(
												-(size.w / 2), -size.h);
										var newIcon = new OpenLayers.Icon(
												'img/poi.png', size, offset);
										var feature = new OpenLayers.Feature(
												markers, point);

										feature.closeBox = closeBox;
										feature.popupClass = popupClass;
										feature.data.popupContentHTML = popupContentHTML;
										feature.data.overflow = (overflow) ? "auto"
												: "hidden";
										feature.data.icon = newIcon;

										var marker = feature.createMarker();

										var markerClick = function(evt) {
											if (!$rootScope.isNewPoiToolEnabled) {												
												showPoiInfo(poi);
											}

											// this = feature
											// markers.removeMarker(this.marker);
											// var newMarker = this.marker;
											// markers.addMarker(newMarker);

											// TODO:
											// removePoiAndMarker(poi,
											// this.marker);

											// alert($rootScope.selectedMap.title);
											// TODO: delete & create points
											// $scope.editMode = true;

											// mapsService.deleteMapPoi($rootScope.selectedMap.id,
											// point.poi.id).success(function(data)
											// {
											// toastr.info("POI eliminado");
											// }).error(function(data) {
											// toastr.info("No se pudo eliminar
											// el POI");
											// });

											OpenLayers.Event.stop(evt);
										};

										marker.events.register("mousedown",
												feature, markerClick);
										marker.events.register("touchstart",
												feature, markerClick);

										marker.poi = poi;
										$rootScope.poiMarkers.push(marker);

										markers.addMarker(marker);
									}

									function showPoiInfo(poi) {
										// Notificar inmediatamente del cambio
										$scope
												.$apply(function() {
													$rootScope.selectedPoi = poi;
													var tmp = document
															.createElement("DIV");
													tmp.innerHTML = poi.description;
													$rootScope.selectedPoi.description = tmp.textContent
															|| tmp.innerText;
												});
									}

									function closePoiInfo() {
										// Notificar inmediatamente del cambio
										$scope.$apply(function() {
											$rootScope.selectedPoi = null;
										});
										// var poiInfoPanelId = 'poi_info';
										// if
										// ($rootScope.isMapEditModeActivated) {
										// // Show editable POI info
										// poiInfoPanelId = 'poi_set';
										// }
									}

									function createNewPoiAndMarker(title,
											description) {
										var point = new OpenLayers.LonLat(
												map.center.lon, map.center.lat)
												.transform(
														new OpenLayers.Projection(
																"EPSG:23030"),
														new OpenLayers.Projection(
																"EPSG:4326"));

										// FIXME: Completar
										title = "";
										description = "";

										var newPoi = {
											"title" : title,
											"description" : description,
											"geometry" : {
												"type" : "Point",
												"coordinates" : [ point.lon,
														point.lat ]
											},
											"updateable":true
										};										
										addPoiToMap(newPoi);
										$rootScope.newPois.push(newPoi);
										$rootScope.selectedPoi = newPoi;
									}

									startRatingSurvey();

								}
							}
						} ]);

/** Main menu directive */
iescitiesAppDirectives
		.directive(
				"menuPanel",
				function() {
					return function($scope) {

						// Close panel
						$scope.closePanel = function(panelId) {
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,-100%,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,-100%,0)";
							}
							document.getElementById(panelId).style.zIndex = 999;
						};

						// Closes all panels
						$scope.closeAllPanels = function() {
							$scope.closePanel('new_map');
							$scope.closePanel('userMapsPanel');
							$scope.closePanel('publicMaps');
							$scope.closePanel('new_user');
							$scope.closePanel('my_profile');
							$scope.closePanel('login');
						};

						// Close all panels and unfade menu
						$scope.fade_back = function() {
							$scope.closeAllPanels();
							if (isWebKit()) {
								document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById('main_menu').style.left = '-14em';
							}
							document.getElementById('black_fader').style.zIndex = '0';
							document.getElementById('black_fader').style.opacity = '0';
						}

						// Toggles main menu
						$scope.toggle_main_menu = function() {
							if (document.getElementById('black_fader').style.zIndex == ""
									|| document.getElementById('black_fader').style.zIndex == "0") {
								// Mostrar menu
								document.getElementById('edit_map').style.zIndex = '100';

								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(14em,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '0';
								}

								document.getElementById('black_fader').style.zIndex = '990';
								document.getElementById('black_fader').style.opacity = '0.5';

							} else {
								// Ocultar menu
								if (isWebKit()) {
									document.getElementById('main_menu').style.webkitTransform = "translate3d(0,0,0)";
								} else {
									document.getElementById('main_menu').style.left = '-14em';
								}
								document.getElementById('black_fader').style.zIndex = '0';
								document.getElementById('black_fader').style.opacity = '0';

								document.getElementById('edit_map').style.zIndex = '1490';
							}
						}

						$scope.showPanel = function(panelId) {
							$scope.fade_back();
							if (isWebKit()) {
								document.getElementById(panelId).style.webkitTransform = "translate3d(0,0,0)";
							} else {
								document.getElementById(panelId).style.transform = "translate3d(0,0,0)";
							}
							document.getElementById(panelId).style.zIndex = 200;
							hideKeyboard();
						};

					};
				});

/** New complaint directive */
iescitiesAppDirectives.directive("ncomplaintPanel", function() {
	return function($scope) {

	};
});

/** Public maps directive */
iescitiesAppDirectives.directive("publicMapsListPanel", function() {
	return function($scope) {

		// Update Public maps list Panel
		// $scope.updatePublicMapsPanel = function(data) {
		// $scope.maps = data;
		// };

	};
});

/** User maps panel directive */
iescitiesAppDirectives.directive("userMapsPanel", function() {
	return function($scope) {

	};
});

/** Profile panel directive */
iescitiesAppDirectives.directive("profilePanel", function() {
	return function($scope) {

	};
});

/** New user panel directive */
iescitiesAppDirectives.directive("newUserPanel", function() {
	return function($scope) {

	};
});

/** Login panel directive */
iescitiesAppDirectives.directive("loginPanel", function() {
	return function($scope) {

	};
});

/** Help info panel directive */
iescitiesAppDirectives.directive("helpInfoPanel", function() {
	return function($scope) {

	};
});
