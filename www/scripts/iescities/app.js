'use strict';

var iescitiesApp = angular.module('iescitiesApp', [ /*'ngRoute'*/, 'ngTouch' ,'angularFileUpload', 'iescitiesAppControllers', 'iescitiesAppServices',
		'iescitiesAppDirectives','toggle-switch' ]);

iescitiesApp.config([ '$compileProvider', function($compileProvider) {
	// $compileProvider.urlSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
	$compileProvider.aHrefSanitizationWhitelist(/^\s*(https?|ftp|mailto|file|tel):/);
} ]);

// iescitiesApp.config([ '$routeProvider', function($routeProvider) {
// $routeProvider.when('/main/', {
// controller : 'MainCtrl',
// templateUrl : 'partials/main.html'
// }).otherwise({
// redirectTo : '/main'
// });
// } ]);

iescitiesApp.filter('range', function() {
	return function(input, total) {
		total = parseInt(total);
		for (var i = 0; i < total; i++)
			input.push(i);
		return input;
	};
});

// iescitiesApp.config(['$httpProvider', function($httpProvider) {
// $httpProvider.defaults.useXDomain = true;
// delete $httpProvider.defaults.headers.common['X-Requested-With'];
// }
// ]);

// Constants
var LANG = "es", GMAPS_VERSION = 3.13, API_KEY = "AIzaSyBTDFDKhfsu7fdFDZjO7kQiF4ace2UfKDU", GLOBAL = "global", LOCAL = "local", LOCAL_ZOOM = 12, SEARCHL_ZOOM = 15, GEOLOCATION_ZOOM = 17, REFRESH_INTERVAL = 12000;

toastr.options = {
	"closeButton" : true,
	"debug" : false,
	"positionClass" : "toast-bottom-full-width",
	"onclick" : null,
	"showDuration" : "300",
	"hideDuration" : "1000",
	"timeOut" : "2000",
	"extendedTimeOut" : "1000",
	"showEasing" : "swing",
	"hideEasing" : "linear",
	"showMethod" : "fadeIn",
	"hideMethod" : "fadeOut"
};

iescitiesApp.filter('reverse', function() {
	return function(items) {
		return items.slice().reverse();
	};
});

iescitiesApp.filter('reverseArrayOnly', function() {
	return function(items) {
		if (!angular.isArray(items)) {
			return items;
		}
		return items.slice().reverse();
	};
});

iescitiesApp.filter('reverseAnything', function() {
	return function(items) {
		if (typeof items === 'undefined') {
			return;
		}
		return angular.isArray(items) ? items.slice().reverse() : // If it is an array, split and reverse it
		(items + '').split('').reverse().join(''); // else make it a string (if it isn't already), and reverse it
	};
});


//IesCities Server
var logType = {
	    LAUNCH: "AppLaunch",  
	    START: "AppStart",
	    STOP: "AppStop",
	    PROSUME: "AppProsume",
	    CONSUME: "AppConsume",
	    OD_CONSUME: "AppODConsume",
	    COLLABORATE: "AppCollaborate",
	    QUESTIONNAIRE: "AppQuestionnaire",
	    DATA_QUERY_INITIATE: "AppDataQueryInitiate",
	    DATA_QUERY_COMPLETE: "AppDataQueryComplete",
	    DATA_QUERY_ERROR: "AppDataQueryError"
	};

// AppLaunch
// App is initiated and a fresh sessionid is generated. The user opens the app.
//
// AppStart
// The App is switched back to.
//
// AppStop
// The App is switched away from.
//
// AppProsume
// The User has supplied content.
//
// AppConsume
// The User has accessed (user supplied) content through the app.
//
// AppODConsume
// The User has accessed Open Data through the app.
//
// AppCollaborate
// The User has performed some collaborative task (created a group, shared an item between groups)
//
// AppQuestionnaire
// The User has submitted a questionnaire.
//
// AppDataQueryInitiate
// The User has made a request from the data store.
//
// AppDataQueryComplete
// The data request has been satisfied.
//
// AppDataQueryError
// Errors were found when accesing the dataset
