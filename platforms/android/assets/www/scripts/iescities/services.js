'use strict';
// API DOC: http://www.zaragoza.es/ciudad/risp/mapas-colaborativos.htm#DetalledeMapa

/* Services */
var iescitiesAppServices = angular.module('iescitiesAppServices', []);

/** Cordova ready listener */
iescitiesAppServices.factory('cordovaReady', function() {
	return function(fn) {

		var queue = [];

		var impl = function() {
			queue.push(Array.prototype.slice.call(arguments));
		};

		document.addEventListener('deviceready', function() {
			queue.forEach(function(args) {
				fn.apply(this, args);
			});
			impl = fn;
		}, false);

		return function() {
			return impl.apply(this, arguments);
		};
	};
});

/** Geolocation service */
iescitiesAppServices.factory('geolocation', [
		'$rootScope',
		'cordovaReady',
		function($rootScope, cordovaReady) {
			return {
				getCurrentPosition : function(onSuccess, onError, options) {
					var success = function() {
						var that = this, args = arguments;

						if (onSuccess) {
							$rootScope.$apply(function() {
								onSuccess.apply(that, args);
							});
						}
					}, error = function() {
						var that = this, args = arguments;
						if (onError) {
							$rootScope.$apply(function() {
								onError.apply(that, args);
							});
						}
					};
					if (document.URL.indexOf('http://') === -1 && document.URL.indexOf('https://') === -1
							&& document.URL.indexOf('file://') === -1) {
						// PhoneGap application
						cordovaReady(function(onSuccess, onError, options) {
							navigator.geolocation.getCurrentPosition(success, error, options);
						});
					} else if (navigator.geolocation) {
						// Web page
						navigator.geolocation.getCurrentPosition(success, error, options);
					}
				}
			};
		} ]);

/** Services info service */
iescitiesAppServices.factory('servicesInfoService', [ '$http', function($http) {
	return {

		getServices : function(callback) {
			return $http.get('http://www.zaragoza.es/api/recurso/open311/services.json').success(callback);
		},

		getDummyServices : function(callback) {
			return $http.get('scripts/iescities/data/getServices.json').success(callback);
		}

	};
} ]);

/** Profile info service */
iescitiesAppServices.factory('profileInfoService', [
		'$http',
		'$rootScope',
		function($http, $rootScope) {
			return {

				// Get profile info
				getProfileInfo : function(callback) {
					return $http.get('scripts/iescities/data/getProfileInfo.json').success(callback);
				},

				// Create new user
				createNewUser : function(profile, schoolType , callback) {
					
					var json_data = JSON.stringify(profile);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/users/addentra/new" + json_data,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : 'http://www.zaragoza.es/api/recurso/users/addentra/new',
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data, status, header, config) {
						console.log("Response: " + JSON.stringify(data));
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
						
						if (schoolType){
							Restlogging.appLog(logType.PROSUME,"new user created of type school");	
						}else {
							Restlogging.appLog(logType.PROSUME,"new user created");	
						}						
						
					}).error(function(data) {
						console.log("Response: " + JSON.stringify(data));
					});
				},

				// Update profile info
				updateProfileInfo : function(profile, callback) {
					var uri = "http://www.zaragoza.es/api/recurso/users/addentra/" + $rootScope.account_id;
					var json_data = JSON.stringify(profile);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "PUT" + "/api/recurso/users/addentra/" + $rootScope.account_id
							+ json_data, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'PUT',
						url : uri,
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data, status, header, config) {
						console.log("Success: " + JSON.stringify(data));
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
					}).error(function(data) {
						console.log("Error: " + JSON.stringify(data));
					});
				}

			};
		} ]);



/** Login service */
iescitiesAppServices.factory('loginService', [
		'$http',
		'$rootScope',		
		function($http, $rootScope) {
			return {

				// Starts login
				login : function(userData, callback) {

					var login = "email=" + userData.email + "&password=" + userData.password;

					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/users/addentra/" + login,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");

					return $http({
						method : 'POST',
						url : 'http://www.zaragoza.es/api/recurso/users/addentra/',
						data : login,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						// 'User-Agent' : 'iescities[movil]',
						// 'Referer' : 'http://www.zaragoza.es/ciudad/'
						}
					}).success(function(data/* , status, header, config */) {
						$rootScope.account_id = data.account_id;
						$rootScope.profileData = data;
						$rootScope.profileData.password = userData.password;
						Restlogging.appLog(logType.CONSUME, "user logged");						
					}).error(function(data) {
						// TODO: Mostrar error
						console.log(JSON.stringify(data));
					});
				}

			};
		} ]);

/** Maps Service */
iescitiesAppServices.factory('mapsService', [
		'$http',
		'$rootScope',
		function($http, $rootScope) {
			return {

				// Gets public maps
				getPublicMaps : function(callback) {
					console.log("mapsService.getPublicMaps()");
					return $http.get('http://www.zaragoza.es/api/recurso/mapas-colaborativos.json?rows=1000',  { headers: { 'Cache-Control' : 'no-cache' } } ).success(callback);
				},

				// Gets map POIs
				getMapPois : function(mapId, callback) {
					console.log("mapsService.getMapPois()");					
					var uri = "http://www.zaragoza.es/api/recurso/mapas-colaborativos/" + mapId + ".json";					
					return $http({
						method : 'GET',
						url : uri						
					}).success(function(data, status, headers, config) {
						console.log("mapsService.getMapPois() - SUCCESS");
						//console.log(JSON.stringify(data));
					}).error(function(data, status, headers, config) {
						console.log("mapsService.getMapPois() - ERROR");
						//console.log(JSON.stringify(data));
					});
				},
				
				// Gets map POIs authenticated
				getMapPoisAuthenticated : function(mapId, callback) {
					console.log("mapsService.getMapPoisAuthenticated()");					
					var uri = "http://www.zaragoza.es/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id +"/" + mapId + ".json";
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id +"/" + mapId,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data, status, headers, config) {
						console.log("mapsService.getMapPoisAuthenticated() - SUCCESS");
						$rootScope.selectedMap.pois = data.result;
						//console.log(JSON.stringify(data));
					}).error(function(data, status, headers, config) {
						console.log("mapsService.getMapPoisAuthenticated() - ERROR");
						console.log(JSON.stringify(data));
						$rootScope.selectedMap.pois = [];
					});
				},

				// Gets user maps
				getUserMaps : function(callback) {
					console.log("mapsService.getUserMaps()" + $rootScope.account_id);
					var uri = "http://www.zaragoza.es/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id;
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "GET" + "/api/recurso/mapas-colaborativos/user/"
							+ $rootScope.account_id,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'GET',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data, status, headers, config) {
						console.log("mapsService.getUserMaps() - SUCCESS");
						console.log(JSON.stringify(data));
					}).error(function(data, status, headers, config) {
						console.log("mapsService.getUserMaps() - ERROR");
					});
				},

				// Saves new map
				saveNewMap : function(map) {
					console.log("mapsService.saveNewMap()");
					// (?srsname=wgs84|utm30n) por defecto wgs84
					var uri = "http://www.zaragoza.es/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id + "?srsname=wgs84";
					var json_data = JSON.stringify(map);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "POST" + "/api/recurso/mapas-colaborativos/user/"
							+ $rootScope.account_id + "?srsname=wgs84" /* (?srsname=wgs84|utm30n) */+ json_data,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'POST',
						url : uri,
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data, status, header, config) {
						console.log(JSON.stringify(data));
						Restlogging.appLog(logType.COLLABORATE,"new map created, type:" + map.type);	
					}).error(function(data) {
						console.log(JSON.stringify(data));
					});
				},

				// Update map
				updateMap : function(map) {
					console.log("mapsService.updateMap()");
					var newMap = {};
					angular.copy(map, newMap);
					//newMap.pois = [];

					if ($rootScope.newPois.length > 0) {
						// angular.copy($rootScope.newPois, newMap.pois);
						newMap.pois = newMap.pois.concat($rootScope.newPois);
					}

					// Add updated pois    *** SE ENVIAN TODOS LOS POIS DEL MAPA ***
/*					if ($rootScope.updatedPois.length > 0) {
						if ($rootScope.newPois.length > 0) {
							newMap.pois = newMap.pois.concat($rootScope.updatedPois);
						} else {
							newMap.pois = $rootScope.updatedPois;
						}
					}*/

					console.log(JSON.stringify("newMap.pois = " + newMap.pois));

					var json_data = JSON.stringify(newMap);
					console.log(json_data);

					// (?srsname=wgs84|utm30n) por defecto wgs84
					var uri = "http://www.zaragoza.es/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id + "/" + newMap.id
							+ "?srsname=wgs84";
					var json_data = JSON.stringify(newMap);
					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "PUT" + "/api/recurso/mapas-colaborativos/user/"
							+ $rootScope.account_id + "/" + newMap.id + "?srsname=wgs84" /* (?srsname=wgs84|utm30n) */+ json_data,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'PUT',
						url : uri,
						data : json_data,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data) {
						console.log("mapsService.updateMap() - SUCCESS");
						Restlogging.appLog(logType.COLLABORATE,"map updated, type: " + newMap.type + ", updated by: " + $rootScope.account_id);
					}).error(function(data) {
						console.log("mapsService.updateMap() - ERROR: " + JSON.stringify(data));
					});
				},

				// Delete map
				// FIXME: No es posible
				deleteMap : function(mapId, callback) {
					console.log("mapsService.deleteMap()");
					var uri = "http://www.zaragoza.es/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id + "/" + mapId;
					var hmac = Crypto
							.HMAC(Crypto.SHA1, "eurohelp" + "DELETE" + "/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id
									+ "/" + mapId, "UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					console.log(uri);
					return $http({
						method : 'DELETE',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}
					}).success(function(data, status, headers, config) {
						console.log("mapsService.deleteMap() - SUCCESS");
						console.log(JSON.stringify(data));
						Restlogging.appLog(logType.COLLABORATE,"map deleted");
					}).error(function(data, status, headers, config) {
						console.log("mapsService.deleteMap() - ERROR");
						console.log(JSON.stringify(data));
					});
				},

				// Delete map poi
				// ids=identificadores_de_puntos_separados_por_comas
				deleteMapPois : function(mapId, deletedPois, callback) {
					var uri = "";
					var deletedPoiIDs = "";
					var deletePois = false;
					deletedPois.forEach(function(poi) {
						if (poi.id != undefined) {
							deletedPoiIDs = deletedPoiIDs.concat(poi.id + ",");
							deletePois = true;
						}
					});

					if (deletePois) {
						deletedPoiIDs = deletedPoiIDs.substring(0, deletedPoiIDs.length - 1);
						uri = "http://www.zaragoza.es/api/recurso/mapas-colaborativos/user/" + $rootScope.account_id + "/" + mapId
								+ "?ids=" + deletedPoiIDs;
					}

					var hmac = Crypto.HMAC(Crypto.SHA1, "eurohelp" + "DELETE" + "/api/recurso/mapas-colaborativos/user/"
							+ $rootScope.account_id + "/" + mapId + "?ids=" + deletedPoiIDs,
							"UpJRoFYTnindPHcmRJvXMs6gf4m9vcGTLGZ1L36hNNGOtEnEyS4J1OBKwWuyUxGEesVg3m");
					return $http({
						method : 'DELETE',
						url : uri,
						headers : {
							'clientId' : 'eurohelp',
							'HmacSHA1' : hmac,
							'Accept' : 'application/json',
							'Content-Type' : 'application/json;charset=UTF-8'
						}

					}).success(callback);
				}

			};
		} ]);

/** IesCities service */
iescitiesAppServices.factory('iesCitiesService', [ '$http', '$rootScope', function($http, $rootScope) {
	return {

		// Iescities server app log
		sendLog : function(message, type) {
			var uri = "http://150.241.239.65:8080/IESCities/api/log/app/stamp/";
			var timestamp = Math.floor(new Date().getTime() / 1000);
			var appid = "zgzcollaborativemaps";
			var session = $rootScope.sessionId;

			uri = uri + timestamp + "/" + appid + "/" + session + "/" + type + "/" + message;
			return $http({
				method : 'GET',
				url : uri,
				headers : {
					'Accept' : 'application/json',
					'Content-Type' : 'application/json;charset=UTF-8'
				}
			}).success(function(data) {
				console.log("iesCitiesService.sendLog() - SUCCESS");
				console.log(JSON.stringify(data));
			}).error(function(data) {
				console.log("iesCitiesService.sendLog() - ERROR");
				console.log(JSON.stringify(data));
			});
		}

	};
} ]);