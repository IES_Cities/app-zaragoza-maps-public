'use strict';

/* Controllers */
var iescitiesAppControllers = angular.module('iescitiesAppControllers', []);

/** Main controller */
// FIXME: Sacar controlador de main.html y crear los necesarios
iescitiesAppControllers
		.controller(
				'MainCtrl',
				[
						'$rootScope',
						'$scope',
						'geolocation',
						'mapsService',
						function($rootScope, $scope, geolocation, mapsService) {
							$scope.gPlace;
							$scope.Math = window.Math;

							// Geolocation button
							$scope.geolocateMe = function() {
								document.getElementById("my_location_btn").className = "my_location_btn_spinner ng-scope";
								document.getElementById("my_location_btn").disabled = true;

								geolocation
										.getCurrentPosition(
												function(position) {
													console
															.log("MainCtrl.getCurrentPosition() - "
																	+ JSON
																			.stringify(position));
													$scope
															.centerMapInPosition(position);
												},
												function(error) {
													toastr
															.error('No ha sido posible geoposicionar su ubicación.');
													document
															.getElementById("my_location_btn").className = "my_location_btn_normal ng-scope";
													document
															.getElementById("my_location_btn").disabled = false;
												}, {
													maximumAge : 3000,
													timeout : 10000,
													enableHighAccuracy : false
												});
							};

							$scope.showEditMapPanel = function() {
								if (isWebKit()) {
									document.getElementById('edit_map_desc').style.webkitTransform = "translate3d(0,0,0)";
								} else {
									document.getElementById('edit_map_desc').style.transform = "translate3d(0,0,0)";
								}
								document.getElementById('edit_map_desc').style.zIndex = 200;
							};

							// Activate map edit mode
							$scope.activateMapEditMode = function() {
								$rootScope.selectedPoi = null;
								$rootScope.isMapEditModeActivated = true;
							};

							// Show new poi tool
							$scope.showNewPoiTool = function() {
								$rootScope.isNewPoiToolEnabled = true;
							};

							// Add new poi to map
							$scope.addNewPoi = function(title, description) {
								$rootScope.isNewPoiToolEnabled = false;
								$scope.addNewPoiToMap();
							};

							// Cancel new poi creation
							$scope.cancelNewPoi = function() {
								$rootScope.isNewPoiToolEnabled = false;
							};

							// Show help panel
							$scope.showHelpPanel = function() {
								$rootScope.helpVisible = true;
								mapsService
										.deleteMap($rootScope.selectedMap.id);
							};

							// Deactivate map edit mode and cancel map changes
							$scope.deactivateMapEditMode = function() {
								// if ($rootScope.newMapTitle != null) {
								// $rootScope.selectedMap.title =
								// angular.copy($rootScope.newMapTitle);
								// $rootScope.selectedMap = $rootScope.editedMap
								// }

								$rootScope.newPois = [];
								$rootScope.updatedPois = [];
								$rootScope.deletedPois = [];
								$rootScope.isMapEditModeActivated = false;
								$scope.cancelMapChanges();
							};

							// Update map
							$scope.updateMap = function(selectedMap) {
								if ($rootScope.deletedPois.length > 0) {

									if ($rootScope.selectedMap.pois) {

										$rootScope.deletedPois.forEach(function(deletedPoi) {
										    
										    $rootScope.selectedMap.pois.forEach(function(poi) {
										    	if (poi.id && poi.id == deletedPoi.id) {
										    	
										    		var i = $rootScope.selectedMap.pois.indexOf(poi);
    												$rootScope.selectedMap.pois.splice(i, 1);

										    	};
										    });

										});

										console.log(JSON.stringify("pois despues de borrado = " + $rootScope.selectedMap.pois));

									}


									// NO SE SI FUNCIONA

									mapsService.deleteMapPois(selectedMap.id,
											$rootScope.deletedPois).success(
											function(data) {
												// TODO:
											}).error(function(data) {
										// FIXME: Error 500, mostrar mensaje de
										// error cuando no
										// se pueda eliminar
										console.log(JSON.stringify(data));
										// toastr.info("Ha ocurrido un error al
										// eliminar un
										// POI");
									});

								}

								mapsService
										.updateMap(selectedMap)
										.success(
												function(data) {
													// console.log(JSON.stringify(data));
													toastr
															.info("El mapa se ha guardado correctamente");

													if ($rootScope.userMap) {
														// Replace local user
														// map with updated one
														var updatedMapId = $rootScope.selectedMap.id;
														var index = $scope.userMaps
																.indexOf($rootScope.selectedMap);
														if (index > -1) {
															$scope.userMaps
																	.splice(
																			index,
																			1);
														}
														$scope.userMaps
																.push(data);
													}

													// Clear updated pois
													$rootScope.newMapTitle = null;
													$rootScope.newPois = [];
													$rootScope.updatedPois = [];
													$rootScope.deletedPois = [];
													$scope.refreshMapData(data);

												})
										.error(
												function(data) {
													toastr
															.info("Ha ocurrido un error al actualizar el mapa");
												});

							};

							$scope.updateSelectedPoi = function() {
								// var updatedPoi = {};
								// angular.copy($rootScope.selectedPoi,
								// updatedPoi);
								if ($rootScope.selectedPoi.title.length < 1) {
									toastr
											.info("El nombre del POI es obligatorio");
									return;
								}
								if ($rootScope.selectedPoi.description.length < 1) {
									toastr
											.info("La descripción es obligatoria");
									return;
								}

								if ($rootScope.selectedPoi.id != null) {
									// EL POI ya existia, mirar si existe en la
									// lisde de POIs
									// modificados
									var index = $rootScope.updatedPois
											.indexOf($rootScope.selectedPoi);
									if (index > -1) {
										// esta en la lista de POIs
										// modificados, actualizar
										// lista
										$rootScope.updatedPois.splice(index, 1);
									}
									$rootScope.updatedPois
											.push($rootScope.selectedPoi);
								}
								$rootScope.selectedPoi = null;

							};

							$scope.deleteSelectedPoi = function() {
								$scope.removePoiFromMap($rootScope.selectedPoi);
							};

							$scope.closePoiInfoPanel = function() {
								$rootScope.selectedPoi = null;
							};

						} ]);

/** Menu controller */
iescitiesAppControllers.controller('menuCtrl', [ '$rootScope', '$scope',
		'mapsService', function($rootScope, $scope, mapsService) {

			// Open new map Panel
			$scope.openNewMapPanel = function() {
				if ($rootScope.account_id != undefined) {
					$scope.showPanel('new_map');
				}
			};

			// Open public maps Panel
			$scope.openPublicMapsPanel = function() {
				$rootScope.isUserPanel = false;
				$scope.showPanel('publicMaps');


				mapsService
			    	.getPublicMaps(function(
			        	data) {
			        $rootScope.publicMaps = data;
			    });


/*				mapsService.getUserMaps().success(function(data) {
					if (data.length > 0) {
						console.log("success" + JSON.stringify(data));
					}
				}).error(function(data) {
					console.log(JSON.stringify(data));
				});*/

			};

			// Open user maps Panel
			$scope.openUserMapsPanel = function() {
				// TODO: poner mapas eidtables
				$rootScope.isUserPanel = true;
				$scope.showPanel('userMapsPanel');
				mapsService.getUserMaps().success(function(data) {
					if (data.length > 0) {
						$scope.updateUserMaps(data);
					}
				}).error(function(data) {
					console.log(JSON.stringify(data));
				});
			};

			// Open new user Panel
			$scope.openNewUserPanel = function() {
				$scope.showPanel('new_user');
			};

			// Open user profile Panel
			$scope.openMyProfilePanel = function() {
				$scope.showPanel('my_profile');
			};

			// Open Login Panel
			$scope.openLoginPanel = function() {
				$scope.showPanel('login');
			};

		} ]);

/** New map controller */
iescitiesAppControllers.controller('newMapCtrl', [ '$rootScope', '$scope',
		'$http', 'mapsService',
		function($rootScope, $scope, $http, mapsService) {

			$scope.publicMap = false;
			$scope.collaborativeMap = false;

			// If map type is set to collaborative then...
			$scope.$watch('collaborativeMap', function() {
				if ($scope.collaborativeMap == true) {
					$scope.publicMap = true;
				}
			}, true);

			// Save map
			$scope.createNewMap = function() {

				if (this.newMapForm.$valid) {
					var mapType = "private";

					if ($scope.collaborativeMap) {
						mapType = "collaborative";
					} else if ($scope.publicMap) {
						mapType = "public";
					}

					var mapBody = {
						"title" : $scope.newMapData.title,
						"type" : mapType
					};

					mapsService.saveNewMap(mapBody).success(function(data) {
						toastr.info("El mapa se ha creado correctamente");
						$scope.userMaps.push(data);
						$scope.closePanel('new_map');
						$scope.refreshMapData(data);
						$rootScope.isUserPanel = true;
						$rootScope.isMapEditable = true;
						$rootScope.userMap = true;
						$scope.newMapData = {};
					}).error(function(data) {
						toastr.info("Ha ocurrido un error");
					});

				} else {
					toastr.info("El titulo es obligatorio");
				}
			};

			$scope.closePanelWithId = function(panelId) {
				$scope.closePanel(panelId);
			};

		} ]);

/** Public maps list controller */
iescitiesAppControllers.controller('publicMapsListCtrl', [ '$rootScope',
		'$scope', function($rootScope, $scope) {

			//
			$scope.showMap = function(map) {
				// FIXME: centrar en funcion de los marcadores,
				// layer.getbounds...
				$scope.centerMapInZaragoza();

				$rootScope.userMap = false;
				$scope.refreshMapData(map);
				$scope.closePanel('publicMaps');
			};

			//
			$scope.closePanelWithId = function(panelId) {
				$scope.closePanel(panelId);
			};

		} ]);

/** Profile controller */
iescitiesAppControllers
		.controller(
				'profileCtrl',
				[
						'$rootScope',
						'$scope',
						'$http',
						'profileInfoService',
						function($rootScope, $scope, $http, profileInfoService) {
							
							// Creates new user
							$scope.createNewUser = function() {
								
								if ($scope.profileData == null) {
									toastr
											.info("El correo y el password son obligatorios");
									return;
								}

								if ($scope.profileData.name == null) {
									toastr
											.info("El nombre es obligatorio");
									return;
								}
								
								if ($scope.profileData.email == null) {
									toastr
											.info("No has introducido el correo electrónico");
									return;
								}

								if ($scope.profileData.password == null) {
									toastr
											.info("No has introducido el password");
									return;
								}
								
								if ($scope.profileData.passwordConfirm == null) {
									toastr
											.info("No has confirmado el password");
									return;
								}
								
								if ($scope.profileData.password != $scope.profileData.passwordConfirm){
									toastr
										.info("El password no coincide");
									return;
								}
								
								var schoolType = false;
								if ($scope.profileData.school == false || $scope.profileData.school == undefined) {
									schoolType = false;
								}	else {
									schoolType = true;						
								}
								
								

								var profileBody = {
									"name" : $scope.profileData.name,
									"email" : $scope.profileData.email,
									"password" : $scope.profileData.password,
									// nif opcional
									"nif" : $scope.profileData.nif									
								};

								profileInfoService
										.createNewUser(profileBody,schoolType)
										.success(
												function(data) {
													$scope
															.closePanel('new_user');
													toastr.options.showDuration = "0";
													toastr.options.hideDuration = "0";
													toastr.options.timeOut = "0";
													toastr
													.success("Se ha registrado correctamente, revise su correo electrónico para activar el servicio y poder utizar la aplicación.");
													toastr.options.showDuration = "300";
													toastr.options.hideDuration = "1000";
													toastr.options.timeOut = "2000";
												})
										.error(
												function(data) {
													toastr
															.info("Error al crear usuario: "
																	+ data.mensaje);
												});

							};

							// Updated current user profile
							$scope.updateProfile = function() {

								var profileBody = {
									"name" : $scope.profileData.name,
									"email" : $scope.profileData.email,
									"password" : $scope.profileData.password,
									// nif opcional
									"nif" : $scope.profileData.nif
								};

								if ($scope.profileData == null) {
									toastr
											.info("El correo y el password son obligatorios");
									return;
								}

								if ($scope.profileData.email == null) {
									toastr
											.info("No has introducido el correo electrónico");
									return;
								}

								if ($scope.profileData.password == null) {
									toastr
											.info("No has introducido el password");
									return;
								}

								if ($scope.profileData.currentPassword != $scope.profileData.password) {
									toastr
											.info("El password actual no es válido");
									return;
								}

								if ($scope.profileData.newPassword != undefined
										&& $scope.profileData.newPassword.length > 0) {
									profileBody.password = $scope.profileData.newPassword;
									$scope.profileData.password = $scope.profileData.newPassword;
								}

								profileInfoService
										.updateProfileInfo(profileBody)
										.success(
												function(data) {
													toastr
															.info("Sus datos de perfil se han modificado correctamente");
													$scope
															.closePanel('my_profile');
												})
										.error(
												function(data) {
													toastr
															.info("No se pudieron actualizar los datos del perfil");
												});
							};

							$scope.closePanelWithId = function(panelId) {
								$scope.closePanel(panelId);
							};

						} ]);

/** Login controller */
iescitiesAppControllers
		.controller(
				'loginCtrl',
				function($scope, $rootScope, loginService, $http, mapsService) {

					$scope.rememberLoginData = window.localStorage
							.getItem("rememberLoginData") == 'true' ? true
							: false;

					// Load stored account credential values
					$scope.loginData = {
						email : window.localStorage.getItem("email"),
						password : window.localStorage.getItem("password")
					};


					$scope.userLogin = function() {
						if ($scope.loginData == null) {
							toastr.info("Email o contraseña no válido");
							return;
						}

						loginService
								.login($scope.loginData)
								.success(
										function(data) {
											console
													.log("logged succesfully...");
											window.localStorage.setItem(
													"rememberLoginData",
													$scope.rememberLoginData);
											if ($scope.rememberLoginData) {
												window.localStorage.setItem(
														"email",
														$scope.loginData.email);
												window.localStorage
														.setItem(
																"password",
																$scope.loginData.password);
											} else {
												window.localStorage.setItem(
														"email", '');
												window.localStorage.setItem(
														"password", '');
											}

											mapsService
													.getUserMaps()
													.success(
															function(data) {
																if (data.length > 0) {
																	toastr
																			.info("Mapas de usuario descargados: "
																					+ data.length);
																	$scope
																			.updateUserMaps(data);
																}
															})
													.error(
															function(data) {
																console
																		.log(JSON
																				.stringify(data));
																// toastr.info("Email
																// o contraseña
																// no válido");
															});

											$scope.closePanel('login');
										})
								.error(
										function(data) {
											toastr
													.info("Email o contraseña no válido");
										});
					};

					$scope.closePanelWithId = function(panelId) {
						$scope.closePanel(panelId);
					};

				});

/** userMapsCtrl */
iescitiesAppControllers.controller('userMapsCtrl', [ '$rootScope', '$scope',
		function($rootScope, $scope) {

			$scope.translateMapType = function(mapType) {
				if (mapType == "public") {
					mapType = "Público"
				} else if (mapType == "private") {
					mapType = "Privado";
				} else {
					mapType = "Colaborativo";
				}
				return mapType;
			};

			$scope.showUserMap = function(map) {
				// FIXME: centrar en funcion de los marcadores,
				// layer.getbounds...
				$scope.centerMapInZaragoza();

				$rootScope.userMap = true;
				$scope.refreshMapData(map);
				$scope.closePanel('userMapsPanel');
			};

			$scope.showDeleteMapDialog = function(mapId) {
				$rootScope.mapIdToDelete = mapId;
				$rootScope.deleteMapDialogVisible = true;
			};

			$scope.closePanelWithId = function(panelId) {
				$scope.closePanel(panelId);
			};

		} ]);

/** editMapCtrl */
iescitiesAppControllers.controller('editMapCtrl', [ '$rootScope', '$scope',
		function($rootScope, $scope) {

			$scope.$watch('selectedMap', function() {
				$scope.publicMap = false;
				$scope.collaborativeMap = false;
				if ($scope.selectedMap.type == "public") {
					$scope.publicMap = true;
				}
				if ($scope.selectedMap.type == "collaborative") {
					$scope.publicMap = true;
					$scope.collaborativeMap = true;
				}
			}, true);

			// If map type is set to collaborative then...
			$scope.$watch('collaborativeMap', function() {
				if ($scope.collaborativeMap == true) {
					$scope.publicMap = true;
				}
			}, true);

			// Update selected map
			$scope.updateSelectedMap = function() {
				// Update map type before close
				if ($scope.publicMap) {
					$rootScope.selectedMap.type = "public";
					if ($scope.collaborativeMap) {
						$rootScope.selectedMap.type = "collaborative";
					}
				} else {
					$rootScope.selectedMap.type = "private";
				}
				$scope.closePanel('edit_map_desc');
			};

			$scope.closePanelWithId = function(panelId) {
				$rootScope.selectedMap.title = $rootScope.mapBackup.title;
				$scope.closePanel(panelId);
			};

		} ]);

/** helpCtrl */
iescitiesAppControllers.controller('helpCtrl', [ '$rootScope', '$scope',
		function($rootScope, $scope) {

			$scope.closeHelpPanel = function() {
				$rootScope.helpVisible = false;
			};

		} ]);

/** dialogCtrl */
iescitiesAppControllers
		.controller(
				'dialogCtrl',
				[
						'$rootScope',
						'$scope',
						'mapsService',
						function($rootScope, $scope, mapsService) {

							// Delete selected map
							$scope.acceptDialog = function() {
								mapsService
										.deleteMap($rootScope.mapIdToDelete)
										.success(
												function(data) {
													console.log(JSON
															.stringify(data));
													toastr
															.info("El mapa se ha eliminado correctamente");

													// Delete map from local
													// user maps
													var index = $scope.userMaps
															.indexOf($rootScope.selectedMap);
													if (index > -1) {
														$scope.userMaps.splice(
																index, 1);
													}

													// Clear selected map
													$scope
															.closePanel('edit_map_desc');
													$rootScope.newPois = [];
													$rootScope.updatedPois = [];
													$rootScope.isMapEditModeActivated = false;
													$rootScope.deleteMapDialogVisible = false;
													$rootScope.selectedMap.title = undefined;
													$rootScope.selectedMap = {};
													$scope.cancelMapChanges();
													$scope
															.closePanel('edit_map_desc');
												})
										.error(
												function(data) {
													// console.log(JSON.stringify(data));
													toastr
															.info("Ha ocurrido un error al tratar de eliminar el mapa, quizás no seas su propietario");
												});
							};

							$scope.cancelDialog = function() {
								$rootScope.deleteMapDialogVisible = false;
							};

						} ]);

/** privacyCtrl */
iescitiesAppControllers.controller('privacyCtrl', [
		'$rootScope',
		'$scope',
		function($rootScope, $scope) {

			$rootScope.privacyVisible = false;

			var isPrivacyAccepted = window.localStorage
					.getItem("isPrivacyAccepted");

			if (isPrivacyAccepted) {
				$rootScope.privacyVisible = false;
			} else {
				$rootScope.privacyVisible = true;
			}

			$scope.acceptPrivacy = function() {
				$rootScope.privacyVisible = false;
				window.localStorage.setItem("isPrivacyAccepted", true);
			};

			$scope.refusePrivacy = function() {
				navigator.app.exitApp();
			};

		} ]);