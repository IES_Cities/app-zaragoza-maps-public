- **Requirements**
	 - Install [SDK de Android](http://developer.android.com/intl/es/sdk/installing/index.html).
	 - Android device or emulador to launch the app.
 - **Install Cordova**
	 - Download and install  [Node.js](https://nodejs.org/en/)
	 - In order to install Cordova module, open a Terminal and execute:
		 - *Windows:* `npm install -g cordova`
		 - *OS X y Linux:* `sudo npm install -g cordova`
 - **Launch the app**
	 - Go to the root directory of the project:
		 - `cd C:\Users\my_user\path_to_the_app`
	 - While the device is physically connected to the device or the emulator running on it, execute:
		 - `cordova run Android`

For more information  [https://cordova.apache.org/docs/en/dev/guide/cli/](https://cordova.apache.org/docs/en/dev/guide/cli/)